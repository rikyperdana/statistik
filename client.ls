if Meteor.isClient

	attr =
		menuList: <[deskriptif variability]>
		uploader: (action) ->
			m \.file, m \label.file-label,
				m \input.file-input, type: \file, name: \csv, onchange: (e) ->
					Papa.parse e.target.files.0, header: true, complete: -> action it
				m \span.file-cta,
					m \span.file-icon, m \i.fa.fa-upload
					m \span.file-label, 'Pilih file .csv'

	comp =
		layout: (comp) -> view: -> m \div,
			m \nav.navbar.is-info,
				role: \navigation, 'aria-label': 'main navigation',
				m \.navbar-brand, m \a.navbar-item,
					href: \/dashboard
					oncreate: m.route.link
					style: "margin-left: 600px"
					_.startCase m.route.get!split(\/)1
			m \.columns,
				m \.column.is-2, m \aside.menu.box,
					m \p.menu-label, \Menu
					m \ul.menu-list, attr.menuList.map ->
						m \li, m \a,
							href: "/#it"
							oncreate: m.route.link
							_.startCase it
				m \.column, m comp

		deskriptif: -> view: -> m \.content,
			m \h5, \Deskriptif
			attr.uploader (res) ->
				disFrek = funcs.dis.frek _.initial(res.data)map ->
					+it[(.0) _.keys res.data.0]
				_.assign state,
					frek: disFrek
					...<[mode med mean]>map ->
						"#it": funcs.dis[it] disFrek
				m.redraw!
			if state.frek then m \div,
				m \table,
					m \thead, <[range fA fR fKA fKR]>map -> m \th, it
					m \tbody, _.reverse(that)map -> m \tr,
						m \td, "#{decimal it.bottom} - #{decimal it.top}"
						m \td, it.count
						m \td, decimal it.rel
						m \td, it.kcount
						m \td, decimal it.krel
				if state.mode.length is 1
					compare = if state.mode.0 < state.mean then <[Kecil Positif]> else <[Besar Negatif]>
					m \ul,
						m \li, "Mode: #{decimal state.mode.0}, Median: #{decimal state.med}, Mean: #{decimal state.mean}"
						m \li, "Mode pada distribusi lebih #{compare.0} dari Mean nya. Mengartikan bahwa sebaran data condong #{compare.1}"
		variability: -> view: -> m \.content,
			attr.uploader (res) ->
				state.zcompare = funcs.var.zcompare _.merge {},
					...(_.keys res.data.0)map (i) -> "#i":
						_.initial(res.data)map -> +it[i]
				m.redraw!
			if state.zcompare then m \div,
				m \h5, 'Mean & SDev'
				m \table,
					m \thead, m \tr, ['', ...(_.keys that.means)]map -> m \th, it
					m \tbody, <[means sdevs]>map (i) -> m \tr, arr =
						m \th, i
						... _.values(that[i])map -> m \td, decimal it
				m \h5, 'Z Scores'
				m \table,
					m \thead, (.map -> m \th, it) arr =
						... _.keys that.zscores
						... (_.keys that.zscores)map -> "z_#it"
					m \tbody, [til that.zscores[_.keys(that.zscores)0]length]map (i) ->
						m \tr, <[val zscore]>map (k) -> _.keys(that.zscores)map (j) ->
							m \td, decimal that.zscores[j][i][k]

	m.route.prefix ''
	m.route document.body, \/dashboard,
		'/dashboard': comp.layout comp.deskriptif!
		'/deskriptif': comp.layout comp.deskriptif!
		'/variability': comp.layout comp.variability!
