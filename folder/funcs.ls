if Meteor.isClient

	@funcs =
		dis:
			frek: (arr) ->
				[lowest, ..., highest] = arr.sort!
				K = Math.ceil (3.3 * Math.log arr.length)
				Interval = Math.ceil (highest - lowest)/K
				arr.reduce do
					(res, inc) -> res.map ->
						unless it.bottom <= inc <= it.top then it
						else _.assign it,
							count: it.count + 1
							rel: (it.count+1)/arr.length*100
					[til K]map ->
						bottom: lowest+(it*Interval)
						top: lowest+((it+1)*Interval)-1
						count: 0
				.map (i, j, k) -> _.assign i,
					kcount: _.sum k.slice(0, j+1)map -> it.count
					krel: _.sum k.slice(0, j+1)map -> it.rel
			mode: (arr) ->
				[..., last] = _.sortBy arr, \count
				arr.filter -> it.count is last.count
				.map -> (it.top - it.bottom)/2+it.bottom
			med: (arr) ->
				[..., last] = _.sortBy arr, \kcount
				mid = last.kcount/2
				loc = arr.find -> it.kcount >= mid
				before = _.reverse(arr)find -> it.top < loc.bottom
				loc.bottom + ((loc.top - loc.bottom + 1)/loc.count)*(mid - before.kcount)
			mean: (arr) -> divs arr =
				sum arr.map -> it.count * ((it.top - it.bottom)/2 + it.bottom)
				_.max arr.map -> it.kcount
		var:
			sdev: (xs) -> Math.sqrt divs arr =
				sum squared diff xs
				xs.length - 1
			cov: -> funcs.var.sdev(it) / mean it
			zscore: (val, bar, sdev) -> (val - bar) / sdev
			zcompare: (vars) ->
				means = _.merge {}, ... _.keys(vars)map -> "#it": mean vars[it]
				sdevs = _.merge {}, ... _.keys(vars)map -> "#it": funcs.var.sdev vars[it]
				zscores = _.merge {}, ... _.keys(vars)map (i) -> "#i": vars[i]map ->
					val: it, zscore: funcs.var.zscore it, means[i], sdevs[i]
				{means, sdevs, zscores}
